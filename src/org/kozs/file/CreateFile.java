package org.kozs.file;

import java.io.IOException;
import java.io.PrintWriter;

import org.kozs.config.SingleVariables;
import org.kozs.gui.GUI;

public class CreateFile {

	public CreateFile(String path) {
		/**
		 * Quick Proccess, could've made a method for this; decideded to make a class.
		 */
		
		try{
		    PrintWriter writer = new PrintWriter("Compile.bat", "UTF-8");
		    writer.println("@echo off");
		    writer.println("COLOR 02");
		    writer.println("title Parabot Compilier - PCC (Created by Kozs)");
		    writer.println("\""+ path + "\\bin\\javac.exe\" -cp \".;pb.jar;pbmini.jar;\" -d ./bin/ src/*.java");
		    writer.println("pause");
		    writer.close();
		    GUI.log.append("Finished File Creation" + SingleVariables.newline);
		   // GUI.btnOutput.setEnabled(true);
		} catch (IOException e) {
		   // do something
		}
	}
}
