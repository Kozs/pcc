package org.kozs.gui;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.kozs.gui.impl.JavaButtonListener;

public class GUI extends JFrame {

	private JPanel contentPane;
	public static JTextArea log;
	public static JButton btnOutput;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setResizable(false);
		setTitle("PCC - Kozs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 327, 234);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnJava= new JButton("Find Java");
		btnJava.setBounds(10, 11, 89, 23);
		contentPane.add(btnJava);
		btnJava.addActionListener(new JavaButtonListener());
		
		
		
		log = new JTextArea();
		log.setEditable(false);
		log.setWrapStyleWord(true);
		log.setBounds(10, 45, 301, 136);
		getContentPane().add(log);
		
		btnOutput = new JButton("Output");
		btnOutput.setBounds(222, 11, 89, 23);
		btnOutput.setEnabled(false);
		contentPane.add(btnOutput);
	}
}
