package org.kozs.gui.impl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.kozs.config.SingleVariables;
import org.kozs.file.CreateFile;
import org.kozs.gui.GUI;

public class JavaButtonListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {
		JOptionPane.showMessageDialog(null, "Please locate your JDK Folder");
	    JFileChooser chooser = new JFileChooser();
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int option = chooser.showSaveDialog(null);
	    if (option == JFileChooser.APPROVE_OPTION)
	    {
	    	GUI.log.append("[Success]" + SingleVariables.newline);
	    	GUI.log.append("[JDK] " + chooser.getSelectedFile().getAbsolutePath() + SingleVariables.newline);
	    	String file = chooser.getSelectedFile().getAbsolutePath();
	    	SingleVariables.path = file;
	    	CreateFile makeBat = new CreateFile(file);
	    }
		
	}

}
